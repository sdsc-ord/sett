# Changelog

All notable changes to this project will be documented in this file.

## [4.2.0](https://gitlab.com/biomedit/sett/-/releases/4%2E2%2E0) - 2023-04-06

[See all changes since the last release](https://gitlab.com/biomedit/sett/compare/4%2E1%2E0...4%2E2%2E0)


### Bug Fixes

- **versioncheck:** Show correct docs URL ([b135ff0](https://gitlab.com/biomedit/sett/commit/b135ff0b16172153738eae90ff70278d2ffeec76)), Closes #434
- **gui:** Skip key approval update when no key in keyring ([8c0f2fb](https://gitlab.com/biomedit/sett/commit/8c0f2fb20596477048796215b120da7a92408839)), Closes #435
- **encrypt:** Improve error message for untrusted keys ([c3ae31d](https://gitlab.com/biomedit/sett/commit/c3ae31d617849d92bb5416cd4f7260a97c3af9fa)), Closes #430

### Features

- Add possibility to paste credentials from clipboard ([4a7a8a5](https://gitlab.com/biomedit/sett/commit/4a7a8a5361c47e6c7145042d96821119a73810a1))
- **s3:** Add support for STS credentials ([8ac7dc9](https://gitlab.com/biomedit/sett/commit/8ac7dc91953f67e7f04f7df21043047f3cdf1c3f)), Closes #439
- Remove sign_encrypted_data option ([f4c9a62](https://gitlab.com/biomedit/sett/commit/f4c9a627c831f4a5bd6f2b5ad831093d61f9d1ec)), Closes #432
# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [4.1.0](https://gitlab.com/biomedit/sett/compare/4.0.0...4.1.0) (2022-11-24)


### Features

* add Python 3.11 support ([4e40c35](https://gitlab.com/biomedit/sett/commit/4e40c359d9ebb32683f425b1e8a28c4bdabd45aa)), closes [#427](https://gitlab.com/biomedit/sett/issues/427)
* **gui/encrypt:** make purpose required when DTR ID is specified ([81390f7](https://gitlab.com/biomedit/sett/commit/81390f724c6d16080d3effefc858e8d2ae1609f2)), closes [#418](https://gitlab.com/biomedit/sett/issues/418)
* **gui/settings:** use 2-column layout ([db206ba](https://gitlab.com/biomedit/sett/commit/db206bac1acb7be6e7019bef40b4c31704b0c9fa)), closes [#426](https://gitlab.com/biomedit/sett/issues/426)
* **gui/transfer:** improve transfer protocol labels and hints ([f6747af](https://gitlab.com/biomedit/sett/commit/f6747af9e7b300c1d5a417240ce6ed0ca71a64ed))
* **gui:** add better support for small screen sizes ([26b5384](https://gitlab.com/biomedit/sett/commit/26b5384885742a4f01260e3576a71457a3d803ee)), closes [#415](https://gitlab.com/biomedit/sett/issues/415)
* **gui:** automatically add the newly encrypted package to the transfer tab ([cd41d3b](https://gitlab.com/biomedit/sett/commit/cd41d3b5402c54ef9b52a54950cafd12a5d7f205)), closes [#414](https://gitlab.com/biomedit/sett/issues/414)
* **gui:** better key approval fetching from portal ([ed8d555](https://gitlab.com/biomedit/sett/commit/ed8d555b5ed7368c0a3af0e6bc0f5aa06a190680)), closes [#424](https://gitlab.com/biomedit/sett/issues/424)
* simplify verification-related options ([f0325d1](https://gitlab.com/biomedit/sett/commit/f0325d1158d009807956fd4dadaa56edcaaa04c5)), closes [#423](https://gitlab.com/biomedit/sett/issues/423)


### Bug Fixes

* **config:** update label 'DCC portal URL' to 'Portal URL' ([a6c9474](https://gitlab.com/biomedit/sett/commit/a6c9474163fa09cfa88553a0b92c5e6587624198))
* **gui settings:** improve handling of mandatory vs. optional fields ([592fc6e](https://gitlab.com/biomedit/sett/commit/592fc6eb9c03f7959fe98b837f530dfa0422607b)), closes [#409](https://gitlab.com/biomedit/sett/issues/409)
* **gui/keys:** refresh secret keys after importing a key from file ([2b56685](https://gitlab.com/biomedit/sett/commit/2b56685a4b0b9928d118091e1ed995022808a051)), closes [#411](https://gitlab.com/biomedit/sett/issues/411)
* **gui/settings:** prevent cursor jumps when editing string values ([49cd6a1](https://gitlab.com/biomedit/sett/commit/49cd6a1764ec40d1647cc0185ff51a2025cf3575)), closes [#410](https://gitlab.com/biomedit/sett/issues/410)
* **gui:** make PGP key import confirmation dialog centered ([57bb592](https://gitlab.com/biomedit/sett/commit/57bb592e1cb6f8e5fcc1c4e32e672b79c413a476)), closes [#405](https://gitlab.com/biomedit/sett/issues/405)
* **gui:** make settings fields work correctly with PySide 6.4.0 ([58a1452](https://gitlab.com/biomedit/sett/commit/58a1452808ca58921da16dbb628bf4be6bc8b02e)), closes [#425](https://gitlab.com/biomedit/sett/issues/425)

## [4.0.0](https://gitlab.com/biomedit/sett/compare/3.7.0...4.0.0) (2022-09-20)


### ⚠ BREAKING CHANGES

* `key_authority_fingerprint` was removed from the config file,
to prepare sett for the new keyserver.

With the new keyserver, the authority is no longer a key, rather some endpoint in `portal`.
An automatic migration step was added to configuration loading process.
So no need to do anything on your side: you configuration file will be automatically migrated.

### Features

* **config:** make PGP key verification configurable ([8ebd287](https://gitlab.com/biomedit/sett/commit/8ebd287bc7fa1fbdea164571d82cf6666aef1c68)), closes [#393](https://gitlab.com/biomedit/sett/issues/393)
* **decrypt:** remove signee_fingerprint argument from verify_metadata_signature ([1f0ebc6](https://gitlab.com/biomedit/sett/commit/1f0ebc688706c346dadb1c0ddf33ace7df827729)), closes [#274](https://gitlab.com/biomedit/sett/issues/274) [#401](https://gitlab.com/biomedit/sett/issues/401)
* **gui/config:** show config file path and a confirmation message on save ([8d3fbe3](https://gitlab.com/biomedit/sett/commit/8d3fbe3fdde59595399b0563a1b18acb024448d5)), closes [#387](https://gitlab.com/biomedit/sett/issues/387) [#357](https://gitlab.com/biomedit/sett/issues/357)
* **keys:** add `request_verify` upon key upload ([0fbb3fa](https://gitlab.com/biomedit/sett/commit/0fbb3fabf0ca564a304409f6e493c616f1cd45eb)), closes [#327](https://gitlab.com/biomedit/sett/issues/327)
* **keys:** remove possibility to request key signature ([1e27f35](https://gitlab.com/biomedit/sett/commit/1e27f35bcb98d68086187d732d9fcb2aa93d9f54)), closes [#327](https://gitlab.com/biomedit/sett/issues/327)
* **keys:** upload/download keys functionality should use Hagrid ([371f55d](https://gitlab.com/biomedit/sett/commit/371f55d7e65a400def3c7161c75306b65e251207)), closes [#327](https://gitlab.com/biomedit/sett/issues/327)
* remove `key_authority_fingerprint` ([e373be6](https://gitlab.com/biomedit/sett/commit/e373be6769e84be2645fb769da646d4291a1ad40)), closes [#392](https://gitlab.com/biomedit/sett/issues/392)
* update key validation method to portal API ([0fcdad3](https://gitlab.com/biomedit/sett/commit/0fcdad3a7e539aebb208be5ebb39e299533f3b0a)), closes [#326](https://gitlab.com/biomedit/sett/issues/326)
* use latest `libbiomedit` features ([a66ddaa](https://gitlab.com/biomedit/sett/commit/a66ddaa579567bd767bd9141f6011f2d82319a8f))


### Bug Fixes

* add config file migration for keyserver URL ([c6448fe](https://gitlab.com/biomedit/sett/commit/c6448fe6634953a3b6f9bfa80b616cabd41acbba)), closes [#400](https://gitlab.com/biomedit/sett/issues/400)
* **commitizen:** `fixup!` NOT accepted by `commitizen` ([1039def](https://gitlab.com/biomedit/sett/commit/1039def10aee12851ed2bf10df77adeae33b3ea6))
* do not refresh or download decryption keys ([5015ecd](https://gitlab.com/biomedit/sett/commit/5015ecda56752f60ad2936e8a99195c805bb0c44)), closes [#398](https://gitlab.com/biomedit/sett/issues/398)
* downloading a non-verified key leads to faulty success message ([cb890ba](https://gitlab.com/biomedit/sett/commit/cb890ba3cc00b421d538191724abc15312880fa6)), closes [#407](https://gitlab.com/biomedit/sett/issues/407)
* inconsistent behavior between offline and verify DTR ID ([eb4d2ee](https://gitlab.com/biomedit/sett/commit/eb4d2ee43bbe956b3f693073b3186ab9c756fafd))
* use unicode characters instead of symbols ([2e54705](https://gitlab.com/biomedit/sett/commit/2e5470525766380d848c060bb43abeb5cce0d6a6)), closes [#399](https://gitlab.com/biomedit/sett/issues/399)
* **utils.config:** remove a race condition on gpg folder creation from multiple processes ([235fadd](https://gitlab.com/biomedit/sett/commit/235faddab628b3b61783de7babe6b6bb0f793fd9))

## [3.7.0](https://gitlab.com/biomedit/sett/compare/3.6.0...3.7.0) (2022-07-22)


### Features

* better error handling for short or non-rsa gpg key signing requests ([a4d5f1c](https://gitlab.com/biomedit/sett/commit/a4d5f1c42605803edeebf05ee6963f5475a9b6ca)), closes [#379](https://gitlab.com/biomedit/sett/issues/379)
* **gui:** add warning in case of ECC keys, do not allow DSA keys to be uploaded ([2247397](https://gitlab.com/biomedit/sett/commit/2247397f58cbfeb65c4de5587f4e76bb03fecc8b))
* **gui:** Hiccups around dcc_portal_url config attribute ([d520618](https://gitlab.com/biomedit/sett/commit/d520618d06e6e084af7a8bf5a2a4f7cbb7309935)), closes [#374](https://gitlab.com/biomedit/sett/issues/374)
* **transfer:** make SFTP buffer size customizable (new default 1MB) ([6afe163](https://gitlab.com/biomedit/sett/commit/6afe163ccf808cf2b3a92b84e240645812da885e)), closes [#372](https://gitlab.com/biomedit/sett/issues/372)


### Bug Fixes

* **gui-settings:** allow : for urls (eg. special ports for testing) ([b9d2728](https://gitlab.com/biomedit/sett/commit/b9d2728d7dac2b18d72814a3054f0055d0a47718))
* **gui:** display sliders for `int` widgets when minimum = 0 ([68da42c](https://gitlab.com/biomedit/sett/commit/68da42cbfae0eed675930125cd700db53858fd46))
* icons should be better rendered in dark mode ([1bc8918](https://gitlab.com/biomedit/sett/commit/1bc8918705bab1413ff78a6850c345733e29adf7)), closes [#317](https://gitlab.com/biomedit/sett/issues/317)
* **log:** remove a race condition on log folder creation from multiple processes ([d01b7b0](https://gitlab.com/biomedit/sett/commit/d01b7b0c0324a530b736cf36ff2c9f218784e19e))

## [3.6.0](https://gitlab.com/biomedit/sett/compare/3.5.3...3.6.0) (2022-06-21)


### Features

* add sett-rs to sett --version ([14de192](https://gitlab.com/biomedit/sett/commit/14de192d2bd805ba287d25fac2cb24c173453b65)), closes [#352](https://gitlab.com/biomedit/sett/issues/352)
* **gui:** add icon and logo ([fa15c74](https://gitlab.com/biomedit/sett/commit/fa15c744ca92760e21efef8523c4c6cfa646817f)), closes [#371](https://gitlab.com/biomedit/sett/issues/371)
* **transfer/S3:** add object storage protocol ([fd4cb11](https://gitlab.com/biomedit/sett/commit/fd4cb11158ca8f89caa4047e6ad5bb6daeecd2e2))


### Bug Fixes

* **cli/transfer:** prompt for missing secrets also when loading a connection from config ([85368a5](https://gitlab.com/biomedit/sett/commit/85368a5c2d95e59ac33f3b8ebaf6eed8846d8263))
* **colorama:** make the colorama dependency optional ([0d3d208](https://gitlab.com/biomedit/sett/commit/0d3d20801117c665746b05838e7956e134702c7c)), closes [#285](https://gitlab.com/biomedit/sett/issues/285)
* improve typing in GUI ([719215d](https://gitlab.com/biomedit/sett/commit/719215ddc8029a94862a44a952ce1588878d1cff)), closes [#373](https://gitlab.com/biomedit/sett/issues/373)
* **workflow.config:** do not raise error when config already exists ([5c352ab](https://gitlab.com/biomedit/sett/commit/5c352abca732ecd2981cfb29f2dcfdf58d1e0c4c))

### [3.5.3](https://gitlab.com/biomedit/sett/compare/3.5.2...3.5.3) (2022-01-17)

### [3.5.2](https://gitlab.com/biomedit/sett/compare/3.5.1...3.5.2) (2022-01-13)


### Bug Fixes

* **cli:** add type override for passphrase argument ([893c800](https://gitlab.com/biomedit/sett/commit/893c80091092ffe5ca5e4e0478016c10836c02bb)), closes [#349](https://gitlab.com/biomedit/sett/issues/349)

### [3.5.1](https://gitlab.com/biomedit/sett/compare/3.5.0...3.5.1) (2021-12-16)

## [3.5.0](https://gitlab.com/biomedit/sett/compare/3.4.0...3.5.0) (2021-12-09)


### Features

* **Connection:** add support for "~" home directory shortcut in 'pkey' parameter of a `connection` ([7f3fb71](https://gitlab.com/biomedit/sett/commit/7f3fb71b5b57bfc26916b6eedbc180d465a7d974))
* Use libbiomedit.lib.secret.Secret instead of core.secret.Secret ([b1cb21d](https://gitlab.com/biomedit/sett/commit/b1cb21d2c9926b6d3f177cf91ed46cc0026b7116))


### Bug Fixes

* **gui:** remove request to persist changes after editing connections ([068ada9](https://gitlab.com/biomedit/sett/commit/068ada931bbf5b0ce67d66ac5740c7b29b4a0962)), closes [#343](https://gitlab.com/biomedit/sett/issues/343)
* **sftp:** generate user-friendly errors for missing/wrong ssh key password ([915a3d5](https://gitlab.com/biomedit/sett/commit/915a3d56bcb09bb9e00235005dc43740fb3e27d6)), closes [#324](https://gitlab.com/biomedit/sett/issues/324)
* **transfer:** improve package name check ([98c1073](https://gitlab.com/biomedit/sett/commit/98c1073e3bb307f3a3644d1efcd70e66595c930e)), closes [#335](https://gitlab.com/biomedit/sett/issues/335)

## [3.4.0](https://gitlab.com/biomedit/sett/compare/3.3.0...3.4.0) (2021-10-18)


### Features

* add Python 3.10 support ([d3a4ca5](https://gitlab.com/biomedit/sett/commit/d3a4ca5da194b6570c63f491a7cd14ac384915ee)), closes [#323](https://gitlab.com/biomedit/sett/issues/323)
* add support for ~ shortcut in config path values ([70a34e5](https://gitlab.com/biomedit/sett/commit/70a34e5ca254b443ccaa90b129f8d5777d40557a)), closes [#284](https://gitlab.com/biomedit/sett/issues/284)
* **gui/keys:** improve UX of the download keys dialog ([8e9c0f5](https://gitlab.com/biomedit/sett/commit/8e9c0f50c2ea91ef9fc114f583df6b8c50219b07)), closes [#157](https://gitlab.com/biomedit/sett/issues/157)
* **transfer:** add package name verification before transfer ([3e2b16d](https://gitlab.com/biomedit/sett/commit/3e2b16dda0437a3df7e767b3f0f6edcd278df48c)), closes [#294](https://gitlab.com/biomedit/sett/issues/294)


### Bug Fixes

* correct writing of connection setting to config file ([36d23b7](https://gitlab.com/biomedit/sett/commit/36d23b76264e24fc56b18bd6627cc7bccbfbccc6)), closes [#325](https://gitlab.com/biomedit/sett/issues/325)
* Disable logging in workflow unittests ([265721f](https://gitlab.com/biomedit/sett/commit/265721f6cef7a30603b9b059e38a00888af5704f))
* **encrypt:** show a user friendly error when encrypting an empty directory ([a65ed5b](https://gitlab.com/biomedit/sett/commit/a65ed5b93709eeb4989c5357c24055ed72f4c5e8)), closes [#321](https://gitlab.com/biomedit/sett/issues/321)
* **gui.console:** in dark mode GUI console output is difficult to read ([251eb00](https://gitlab.com/biomedit/sett/commit/251eb0066873240ba3654488d555037e96eafecc)), closes [#313](https://gitlab.com/biomedit/sett/issues/313)

## [3.3.0](https://gitlab.com/biomedit/sett/compare/3.2.0...3.3.0) (2021-09-21)


### Features

* **decrypt:** remove initial checksum check to speed up decrypt workflow ([37d51aa](https://gitlab.com/biomedit/sett/commit/37d51aae67a93cb5bc3e7d38405f4bfc8e9a7f70)), closes [#295](https://gitlab.com/biomedit/sett/issues/295)
* Include python version in runtime info and crash report ([1b3f637](https://gitlab.com/biomedit/sett/commit/1b3f6373e391272e2eec8eedb0d5bf0b41ecd248))


### Bug Fixes

* **benchmark:** update script to match current sett interface ([4408e11](https://gitlab.com/biomedit/sett/commit/4408e116d4f867f4a7e9c77283f63362ea903766))
* log runtime info consistently in cli, gui, logfile, and error_report ([fe58290](https://gitlab.com/biomedit/sett/commit/fe58290d36e83ff39ebcadcdc2e000ca8154c210)), closes [#299](https://gitlab.com/biomedit/sett/issues/299)
* Reenable PGP keypair generation in GUI ([5543da5](https://gitlab.com/biomedit/sett/commit/5543da5a9bd7581f33117e8107467d0d13916cd4))
* Satisfy new pylint lint consider-using-f-string ([73ba027](https://gitlab.com/biomedit/sett/commit/73ba027063760583293c1f2ac1a00920aa597022))
* **utils.progress:** use utf-8 encoding only for text files ([4b3ba59](https://gitlab.com/biomedit/sett/commit/4b3ba5909531bd81221f0b0c3ba8b891ac3d6cb8))
* **version check:** Fix crash when pypi is not reachable ([92cf3ae](https://gitlab.com/biomedit/sett/commit/92cf3aecabeeea535c18b46a19778903c88e33b0))

## [3.2.0](https://gitlab.com/biomedit/sett/compare/3.1.0...3.2.0) (2021-08-11)


### Features

* add option to enable/disable key auto-download ([24bd1b7](https://gitlab.com/biomedit/sett/commit/24bd1b78ded6e20fb5ae45d0a95514ce10c16cd1)), closes [#143](https://gitlab.com/biomedit/sett/issues/143)
* **checksum:** run input and output file checksumming in parallel ([f1fe2ff](https://gitlab.com/biomedit/sett/commit/f1fe2ffd42191f14e2cf840e9adb292c28cf99c4)), closes [#291](https://gitlab.com/biomedit/sett/issues/291)
* **logging:** add process id to file log ([e5392c7](https://gitlab.com/biomedit/sett/commit/e5392c7d512000ba7eb28298cba2531af015bee7)), closes [#293](https://gitlab.com/biomedit/sett/issues/293)
* **sftp:** use sett-rs for transferring files over sftp ([f41b933](https://gitlab.com/biomedit/sett/commit/f41b933e30dce9c82f77f2a9790e1dc8a6bca886)), closes [#267](https://gitlab.com/biomedit/sett/issues/267)
* show file size and compression ratio in logs ([fae6f7f](https://gitlab.com/biomedit/sett/commit/fae6f7f910cbfffbfc3e5ac6c35d0e9809416346)), closes [#292](https://gitlab.com/biomedit/sett/issues/292)


### Bug Fixes

* **crypt:** add legacy GnuPG signature check support ([3eb7c2b](https://gitlab.com/biomedit/sett/commit/3eb7c2b518b5708b06ead876671f04e428e79030)), closes [#297](https://gitlab.com/biomedit/sett/issues/297)
* **encrypt:** remove 'offline' argument that has become obsolete ([6f0c9fa](https://gitlab.com/biomedit/sett/commit/6f0c9faa908dd2c557ead18f3160bcb8457ae71b))
* **logging:** prevent log level name overwriting by emoji formatter ([456344d](https://gitlab.com/biomedit/sett/commit/456344d5ae0d595a754f37213c656690bd04b105))
* remove direct keyerver errors handling ([432b75e](https://gitlab.com/biomedit/sett/commit/432b75e43ad616621a479342140e4e39418a8a74)), closes [#275](https://gitlab.com/biomedit/sett/issues/275)
* **sftp.Protocol:** correctly handle empty strings and missing host port ([438373e](https://gitlab.com/biomedit/sett/commit/438373ed8c5a9e2fae02bb9782033120bf909a6f))

## [3.1.0](https://gitlab.com/biomedit/sett/compare/3.0.0...3.1.0) (2021-07-22)


### Features

* **decrypt:** check password before attempting decryption ([0f41042](https://gitlab.com/biomedit/sett/commit/0f41042fc99cf10ff1fd9483dc7a1eacaed572bc)), closes [#279](https://gitlab.com/biomedit/sett/issues/279)
* Warn about out of storage ([8acfb58](https://gitlab.com/biomedit/sett/commit/8acfb582c1b94b1558698511dae2e1feb5d26ad7))


### Bug Fixes

* **config:** update user config file migration to remove temp_dir argument ([da67947](https://gitlab.com/biomedit/sett/commit/da67947ee3dff13bdf7c7ef49a23dfc4d1a1ce6d))
* **gui:** disable OK button in dialog when password is empty ([6fd0104](https://gitlab.com/biomedit/sett/commit/6fd010408eca85ba5a3d95cf4236119c768c159d))
* **gui:** improve GPG key generation user input parsing ([4e434b0](https://gitlab.com/biomedit/sett/commit/4e434b057690de38d0123eef8be883743d63cb58))
* **gui:** refresh key info in keys tab upon key update ([4fae07d](https://gitlab.com/biomedit/sett/commit/4fae07dc69876d82aa8fcae77e6b1015a49cd9c6)), closes [#287](https://gitlab.com/biomedit/sett/issues/287)
* Warn about out of storage: only display comment about ignoring this warning if ignoring is not already activated ([b534e60](https://gitlab.com/biomedit/sett/commit/b534e60fb8223886f7bec8c65c546a1006085d07))

## [3.0.0](https://gitlab.com/biomedit/sett/compare/2.8.1...3.0.0) (2021-06-21)


### ⚠ BREAKING CHANGES

* **core.portal_api:** new project_code field is used instead of now deprecated project_id
* package file format is changed from tar to zip

### Features

* auto-download authority key on startup. Use full fingerprint ([0457258](https://gitlab.com/biomedit/sett/commit/0457258b5af60f45be6d4e5ab8125b4dfcfb2779)), closes [#241](https://gitlab.com/biomedit/sett/issues/241) [#227](https://gitlab.com/biomedit/sett/issues/227)
* run encryption workflow without temporary files ([8743788](https://gitlab.com/biomedit/sett/commit/87437889b8d70ecb24db5dfed7ae578828b72fae))
* **config:** make output file name suffix configurable ([e84a03f](https://gitlab.com/biomedit/sett/commit/e84a03fc4874c8a21e5bd8f5afab72679d4ac8ab))
* **encrypt:** add more options to output file name ([81cb688](https://gitlab.com/biomedit/sett/commit/81cb68812e9a6cf97e0fac2ac66d5ab497ab9a1b)), closes [#258](https://gitlab.com/biomedit/sett/issues/258)
* **log:** use colors and emojis to make logs more readable ([e9d6d94](https://gitlab.com/biomedit/sett/commit/e9d6d949e41eb8f3b52289e9de595a905617b9c7)), closes [#259](https://gitlab.com/biomedit/sett/issues/259)


### Bug Fixes

* **gui:** perform authority key download in a separate thread ([c1727b9](https://gitlab.com/biomedit/sett/commit/c1727b911f3dd9d7d943c460b1dfeb383ce19612)), closes [#280](https://gitlab.com/biomedit/sett/issues/280)
* add migration for key_ID to fingerprint ([131f949](https://gitlab.com/biomedit/sett/commit/131f94904ddcef1e2e7202e3df0f1a2b7e4f3dc6)), closes [#282](https://gitlab.com/biomedit/sett/issues/282)
* **core.portal_api:** use new Portal API ([e113905](https://gitlab.com/biomedit/sett/commit/e113905662fa7f8b76963720b491c43c07637eb3))
* **gui:** remove deprecation warning ([d3b70e3](https://gitlab.com/biomedit/sett/commit/d3b70e35f480be22d41ef3c93dd4253e27441885)), closes [#270](https://gitlab.com/biomedit/sett/issues/270)
* **gui/keys:** show error message when searching for keys and service is unreachable ([6a60318](https://gitlab.com/biomedit/sett/commit/6a60318765859a3c2014ae03da3f87048aab5955)), closes [#230](https://gitlab.com/biomedit/sett/issues/230)

### [2.8.1](https://gitlab.com/biomedit/sett/compare/2.8.0...2.8.1) (2021-05-18)


### Bug Fixes

* **gui:** show error messages in console and error reports ([1419f01](https://gitlab.com/biomedit/sett/commit/1419f0180a892d992c187b1e904647b60d361ef5)), closes [#268](https://gitlab.com/biomedit/sett/issues/268)
* **gui/keys:** fix key download and generation issue ([4cd5eb9](https://gitlab.com/biomedit/sett/commit/4cd5eb986d588e7727932f29229593b5a74196b5)), closes [#269](https://gitlab.com/biomedit/sett/issues/269)

## [2.8.0](https://gitlab.com/biomedit/sett/compare/2.7.0...2.8.0) (2021-05-17)


### Features

* Automatically generate an error report on any exception ([b3a6ce9](https://gitlab.com/biomedit/sett/commit/b3a6ce9115f8f6d560b6c08684aee1db6ea57635))
* **config:** Add option to disable logging to file ([388cfb8](https://gitlab.com/biomedit/sett/commit/388cfb8006db0d8c1668f0240be14b559b6bc611))
* **gui:** use icons ([719a986](https://gitlab.com/biomedit/sett/commit/719a98670970e40fda54fe0146a8c2b7f9aee3ca)), closes [#240](https://gitlab.com/biomedit/sett/issues/240)
* **gui:** use Qt Resource System ([e76b967](https://gitlab.com/biomedit/sett/commit/e76b9677ac5c33c80eb4a517aa0478e4f8415fce))
* **workflow:** time the workflows ([c0ebcca](https://gitlab.com/biomedit/sett/commit/c0ebccaaf23a75daf173988cd5830a2fb3f64cf3)), closes [#238](https://gitlab.com/biomedit/sett/issues/238)


### Bug Fixes

* **gui:** remove ugly gradient from toolbar on macos ([8fd6f10](https://gitlab.com/biomedit/sett/commit/8fd6f10d155d82865b86eb90ba280bec22785dfc))
* **gui.decrypt:** unify compression widget experience between encrypt and decrypt ([eda60c5](https://gitlab.com/biomedit/sett/commit/eda60c5fdb5a934b76e883dbbb73b553eb89234a)), closes [#192](https://gitlab.com/biomedit/sett/issues/192)

## [2.7.0](https://gitlab.com/biomedit/sett/compare/2.6.0...2.7.0) (2021-03-10)


### Features

* **cli:** Prompt for password if pkey is specified but pw is missing ([c76efd3](https://gitlab.com/biomedit/sett/commit/c76efd3a20d21a41cc41a61cb5815aca2dea75bb))
* **gui:** add file drag and drop support ([afea6f2](https://gitlab.com/biomedit/sett/commit/afea6f2c457f3e613ff80239535cc38dfd043164)), closes [#236](https://gitlab.com/biomedit/sett/issues/236)
* **gui:** use PySide6 ([bd32009](https://gitlab.com/biomedit/sett/commit/bd32009c6c2dc6abfac68dd00d7ce63ac2bf3d89))
* **gui.encrypt:** replace compress checkbox with compression level slider ([5487fe0](https://gitlab.com/biomedit/sett/commit/5487fe09ae6b4455771fbc0b7cbddc078d04d5a2)), closes [#233](https://gitlab.com/biomedit/sett/issues/233)
* **gui.keys_tab:** show key signature dialog after key generation ([649ded5](https://gitlab.com/biomedit/sett/commit/649ded5f29e00c11584f4963e3e3f43c35b26fd8)), closes [#237](https://gitlab.com/biomedit/sett/issues/237)
* **workflows.request_sigs:** upload key before sending signing request ([a1deb9a](https://gitlab.com/biomedit/sett/commit/a1deb9a3ad2532cd3323eee20c91f58a21af4a83))


### Bug Fixes

* **gui:** update deprecated PySide regex calls ([9410bf6](https://gitlab.com/biomedit/sett/commit/9410bf6ddfaaf8885facd1fcbbdc6136e742e1f5))

## [2.6.0](https://gitlab.com/biomedit/sett/compare/2.5.0...2.6.0) (2021-01-29)


### Features

* **config:** manage connection profiles in GUI ([0b5a6c4](https://gitlab.com/biomedit/sett/commit/0b5a6c4ddb48713f18167d71b1c7eaafcb17abbb)), closes [#186](https://gitlab.com/biomedit/sett/issues/186)
* **gui:** add settings tab ([3ee09cd](https://gitlab.com/biomedit/sett/commit/3ee09cd455b4998327faa6ec528919fd23c56d85)), closes [#69](https://gitlab.com/biomedit/sett/issues/69)
* **gui/settings:** show dialog when closing sett if there are settings that are not persisted ([c618df9](https://gitlab.com/biomedit/sett/commit/c618df9b5962bbbf4e9d7a063c3072b5ff88f296)), closes [#69](https://gitlab.com/biomedit/sett/issues/69)
* **workflows.decrypt:** Allow using decrypt_tar with progress=None ([29c88ae](https://gitlab.com/biomedit/sett/commit/29c88ae69ba8ca6d0679cfdff7d4a1999e8de88b))
* **workflows.encrypt:** also accept folders for output_name ([1e50ce0](https://gitlab.com/biomedit/sett/commit/1e50ce002999622bd426c562fd6e93dad1b7d5fb))
* **workflows.encrypt:** return output_name ([2139a9e](https://gitlab.com/biomedit/sett/commit/2139a9e9fb16b95c4b457a724e6ccdedb381b3ef))
* add support for SETT_CONFIG_FILE environmental variable ([cba9274](https://gitlab.com/biomedit/sett/commit/cba927446c2b4bb31e201faca603ad086dd68b72)), closes [#214](https://gitlab.com/biomedit/sett/issues/214)


### Bug Fixes

* **config:** create missing config file when saving connections ([17e6fa7](https://gitlab.com/biomedit/sett/commit/17e6fa7ca30a80fe3198bcfe908851088574ae6e)), closes [#225](https://gitlab.com/biomedit/sett/issues/225)
* **config:** handle broken user configuration files ([66782d1](https://gitlab.com/biomedit/sett/commit/66782d1468464b309e9b799702025ded142d92af)), closes [#220](https://gitlab.com/biomedit/sett/issues/220)
* **core.archive:** Remove backwards compatibility to 0.9 (enforce detached signature) ([b50fea1](https://gitlab.com/biomedit/sett/commit/b50fea11f9a31efe3c01eccac5176811883df10c))
* **gui.transfer_tab:** clear missing fields when switching between profiles ([1fef383](https://gitlab.com/biomedit/sett/commit/1fef383b9348df8596b73801a997c251f9c74404))
* **protocol.sftp:** Fix follow up error when connection fails ([f276473](https://gitlab.com/biomedit/sett/commit/f27647379b712851604b8de89f2192045dd529d5))
* **typo:** 'filename_candidates' instead of 'filename_canditates' ([e1c5b35](https://gitlab.com/biomedit/sett/commit/e1c5b35d8b1dec78601f3caea545ac2965ea1d4e)), closes [#222](https://gitlab.com/biomedit/sett/issues/222)
* **version check:** make version upgrade message more explicit ([de4de8f](https://gitlab.com/biomedit/sett/commit/de4de8f121b8285ceecdb98c22af532014567094)), closes [#213](https://gitlab.com/biomedit/sett/issues/213) [#217](https://gitlab.com/biomedit/sett/issues/217)
* disable key upload and signature request in offline mode ([8a431d5](https://gitlab.com/biomedit/sett/commit/8a431d5551e36146dd3f53faf68c7b72e1ec3ea3)), closes [#205](https://gitlab.com/biomedit/sett/issues/205)

## [2.5.0](https://gitlab.com/biomedit/sett/compare/2.4.0...2.5.0) (2020-11-30)


### Features

* **encrypt:** add compression level setting as option to sett config and CLI. Default level is 5 ([d7bf8f0](https://gitlab.com/biomedit/sett/commit/d7bf8f058aa3fbbaa96c572b3b7aa4bd173a242a)), closes [#202](https://gitlab.com/biomedit/sett/issues/202)


### Bug Fixes

* **crypt:** handle the possibility of empty email addresses in key ID introduced in gpg-lite 0.7.2. ([1dd5922](https://gitlab.com/biomedit/sett/commit/1dd592238eea2c1a45f3e70890d3c460d8b19ea1))
* **decrypt:** attach progress to tar file extraction ([721332a](https://gitlab.com/biomedit/sett/commit/721332aed01d89fc7359e9f8d3f51c2647780c2a))

## [2.4.0](https://gitlab.com/biomedit/sett/compare/2.3.0...2.4.0) (2020-11-05)


### Features

* **config:** add possibility to specify a default output dir for encrypt/decrypt in configuration file ([c90334e](https://gitlab.com/biomedit/sett/commit/c90334e27b11cb717263916ab857d08765eb3e38))


### Bug Fixes

* **config:** remove pypi url migration ([05e830c](https://gitlab.com/biomedit/sett/commit/05e830c71c011e2409922ec91257ab2489232bdf))
* **gui:** improve exit message ([abf1b55](https://gitlab.com/biomedit/sett/commit/abf1b55e2069eda34426794d4845e9568f849d2f)), closes [#167](https://gitlab.com/biomedit/sett/issues/167)
* **progress:** remove unused 'set_label' ([1d7653b](https://gitlab.com/biomedit/sett/commit/1d7653baf773bbbea06f886f0ca7b623cc82e27f))
* **protocols.sftp:** better error message for a missing remote directory ([949b004](https://gitlab.com/biomedit/sett/commit/949b0043d22ee5d75bf031a8f3ab88dfc36a8727)), closes [#185](https://gitlab.com/biomedit/sett/issues/185)
* **utils.progress:** empty files do not update progress bar ([698c9d6](https://gitlab.com/biomedit/sett/commit/698c9d64aec1bc60292c1e05b38244d862dfea8a))

## [2.3.0](https://gitlab.com/biomedit/sett/compare/2.2.0...2.3.0) (2020-10-23)


### Features

* **workflow:** stream data between stages (no intermediate files) ([e289669](https://gitlab.com/biomedit/sett/commit/e2896694aa50bea09f35cb3583352edac103bc27)), closes [#84](https://gitlab.com/biomedit/sett/issues/84)


### Bug Fixes

* **cli.progress:** fix progress bar refreshing ([21530dd](https://gitlab.com/biomedit/sett/commit/21530dd96fe3804dd87833b46150546f0dda824e))
* **config:** specify optional parameters as optional so 'null' is properly interpreted in config.json ([ce42575](https://gitlab.com/biomedit/sett/commit/ce425757869d34bff2d921b3934bfe0f20a7ac13))
* **gui:** always show progress bar ([7b3bea4](https://gitlab.com/biomedit/sett/commit/7b3bea4436649aca5d49cbc7dcd368086adb4e2d))
* **GUI:** mark required fields as such ([015e20a](https://gitlab.com/biomedit/sett/commit/015e20a87a78bae9d1645c3ca39153eb20e4278c)), closes [#191](https://gitlab.com/biomedit/sett/issues/191)
* **workflows.decrypt:** fix progress bar behavior ([ff2b4f5](https://gitlab.com/biomedit/sett/commit/ff2b4f59c7be1525ae85f5bd1b459e38141012f4)), closes [#156](https://gitlab.com/biomedit/sett/issues/156)

## [2.2.0](https://gitlab.com/biomedit/sett/compare/2.1.0...2.2.0) (2020-10-07)


### Features

* **CLI:** Change keyword list arguments from --flag arg1 arg2 ... to --flag arg1 --flag arg2 ([b6f66e2](https://gitlab.com/biomedit/sett/commit/b6f66e2d24e5089e26fb4433d948463b2b607311)), closes [#154](https://gitlab.com/biomedit/sett/issues/154)
* **CLI:** Change negated boolean flags to --no-flag in favor of --flag=false ([3489c2c](https://gitlab.com/biomedit/sett/commit/3489c2c93c1539c064bb90373cae4fa4116a83e8))
* **CLI:** Cleanup: remove --offline flag ([c66d1b6](https://gitlab.com/biomedit/sett/commit/c66d1b6626b8afe0093dba26e542dc6ebbbaae8a))
* **CLI:** Introduce veryfy_dtr in encrypt (set user friendly defaults) ([7bfcf20](https://gitlab.com/biomedit/sett/commit/7bfcf20a01694a6de1dd7e33acff004034cde498))
* **decrypt:** always use unique output folder names ([136285d](https://gitlab.com/biomedit/sett/commit/136285df60b844090c20f3b0fb950d4bc91647df))


### Bug Fixes

* **cli:** 'e.stderr' is encoded ([0de19d9](https://gitlab.com/biomedit/sett/commit/0de19d985dd3f8295c3e58b3f0744b7a28f39e6d))
* **cli:** non-optional positional args are marked as required ([cebc2a6](https://gitlab.com/biomedit/sett/commit/cebc2a6a8793b135c8ca11ca21618f8d114425e0)), closes [#105](https://gitlab.com/biomedit/sett/issues/105)
* **decrypt:** cleanup output folder upon exception ([45ae23f](https://gitlab.com/biomedit/sett/commit/45ae23fbddcae20c684a9596bde2721a9d988216)), closes [#180](https://gitlab.com/biomedit/sett/issues/180)
* **gui:** add missing status tips ([ab593d8](https://gitlab.com/biomedit/sett/commit/ab593d8e591ebefe27c99bc0aeda3f814735f64a)), closes [#147](https://gitlab.com/biomedit/sett/issues/147)
* **GUI:** Focus should move to pressed button ([da1c5e6](https://gitlab.com/biomedit/sett/commit/da1c5e6e77d060ec0ef630191fbbac99cdfc7c68))
* **GUI:** Missing 'repaint' for Mac OS X ([a235e77](https://gitlab.com/biomedit/sett/commit/a235e77e49cb7629c6211a617ad37a4bf925fbe7)), closes [#165](https://gitlab.com/biomedit/sett/issues/165)
* **GUI:** Refactor handling of mandatory fields and button activation ([5574dec](https://gitlab.com/biomedit/sett/commit/5574decc08cdd0c355713cb2e00e867cc47a2dd3))

## [2.1.0](https://gitlab.com/biomedit/sett/compare/2.0.1...2.1.0) (2020-09-08)


### Features

* **cli:** replace passphrase-file with passphrase-cmd ([798b4e7](https://gitlab.com/biomedit/sett/commit/798b4e739dc39f5d039e1cb25ba46b54de1d5985)), closes [#176](https://gitlab.com/biomedit/sett/issues/176)
* **transfer:** add option to disable DTR ID verification ([f86ec9a](https://gitlab.com/biomedit/sett/commit/f86ec9a096d549bc35d1ef8764e18a530b842c10)), closes [#175](https://gitlab.com/biomedit/sett/issues/175)


### Bug Fixes

* explicitly re-raise exceptions using the 'from' keyword ([0240039](https://gitlab.com/biomedit/sett/commit/02400394d3d1a55308963205481b8301e461cfba))
* **config:** remove trailing '/' from 'dcc_portal_url' if present ([8b21326](https://gitlab.com/biomedit/sett/commit/8b213262199eeb4f669e6c87121645a7766978bf)), closes [#173](https://gitlab.com/biomedit/sett/issues/173)

### [2.0.1](https://gitlab.com/biomedit/sett/compare/2.0.0...2.0.1) (2020-08-20)


### Bug Fixes

* use 'DTR' instead of 'Transfer' ([c16cad3](https://gitlab.com/biomedit/sett/commit/c16cad3a52a82a64e1177f5c4df9d058f8b7f6d2)), closes [#169](https://gitlab.com/biomedit/sett/issues/169)
* **test:** Silence logger in test.utils.test_log ([616705a](https://gitlab.com/biomedit/sett/commit/616705a86e1a128e3d58decb5b07bbfc39615e26))
* **workflows.encrypt:** enforce user to specify a purpose ([90aa7ec](https://gitlab.com/biomedit/sett/commit/90aa7ecc137cb9b3536605fa2cc6b0e2ddc99ad3)), closes [#168](https://gitlab.com/biomedit/sett/issues/168)

## [2.0.0](https://gitlab.com/biomedit/sett/compare/1.3.0...2.0.0) (2020-08-18)


### ⚠ BREAKING CHANGES

* transfer_id replaces project_id for identifying data
    package destinations.

    Data packages created with this version will no longer support
    packages using project_id.

### Features

* change project_id to transfer_id ([83d0d58](https://gitlab.com/biomedit/sett/commit/83d0d58c282499a8bf528d2a3198d106caddf919)), closes [#128](https://gitlab.com/biomedit/sett/issues/128)

## [1.3.0](https://gitlab.com/biomedit/sett/compare/1.2.1...1.3.0) (2020-08-14)


### Features

* show/log version (sett, dependencies, OS) when running sett ([fbc6ea6](https://gitlab.com/biomedit/sett/commit/fbc6ea6efc454b996ae5305e9f04076424e89ccc)), closes [#161](https://gitlab.com/biomedit/sett/issues/161)
* **cli:** add --passphrase-file option ([fa08412](https://gitlab.com/biomedit/sett/commit/fa08412cfe69297623907f7a55326810d3898b8d)), closes [#146](https://gitlab.com/biomedit/sett/issues/146)
* **core.filesystem:** Turn exception if files cannot be deleted into warning ([a514d6d](https://gitlab.com/biomedit/sett/commit/a514d6de0866d9bcf38455366bdddf6624b87713)), closes [#118](https://gitlab.com/biomedit/sett/issues/118)
* **gui:** make gui window quit confirmation optional ([9281a77](https://gitlab.com/biomedit/sett/commit/9281a77f8ae8917976b1d48f613c90977df0f818)), closes [#144](https://gitlab.com/biomedit/sett/issues/144)


### Bug Fixes

* **cli:** fix partial to not override keywords field ([a9c397f](https://gitlab.com/biomedit/sett/commit/a9c397f98b33d72c8a514094fd4c65c73632cb96))
* **core.checksum:** Support for upper case checksums in the checksums file ([800289a](https://gitlab.com/biomedit/sett/commit/800289ab95b49bddddc4bf5941924838b5013643))
* **core.crypt.verify_metadata_signature:** Support windows encoded ascii armored signatures ([81bbca9](https://gitlab.com/biomedit/sett/commit/81bbca9d250c161424f9f8027511fd8d5b659525))
* **gui:** fix header repainting issue on MacOS X ([04491cd](https://gitlab.com/biomedit/sett/commit/04491cda16352ec7dd3695f492acfd979e181d6c))
* **gui:** repaint after having cleared the text box ([75ffb5c](https://gitlab.com/biomedit/sett/commit/75ffb5c063b82e4d1a1053759d44902ef16f6831)), closes [#111](https://gitlab.com/biomedit/sett/issues/111)
* **gui:** show warning for update,upload,delete,sign key when no key is selected ([858a70c](https://gitlab.com/biomedit/sett/commit/858a70cb30e6f623d62ce8d2e583eb3931cd1e8f)), closes [#140](https://gitlab.com/biomedit/sett/issues/140)
* **gui:** split 'app.py' into smaller pieces ([2dd51c1](https://gitlab.com/biomedit/sett/commit/2dd51c179d5676b4228db7325fe946f2950d0797)), closes [#158](https://gitlab.com/biomedit/sett/issues/158)
* **gui.keys:** use existing workflows ([e2b14ff](https://gitlab.com/biomedit/sett/commit/e2b14ffebe5fc9baba608699a4428b79c9c29f58))
* **workflows:** verify minimal key length ([75c66b9](https://gitlab.com/biomedit/sett/commit/75c66b9910acd944b8222cebfadd3553a5eef20d)), closes [#142](https://gitlab.com/biomedit/sett/issues/142)

### [1.2.1](https://gitlab.com/biomedit/sett/compare/1.2.0...1.2.1) (2020-07-30)


### Bug Fixes

* **core.crypt:** Fix key refreshing ([fcec85a](https://gitlab.com/biomedit/sett/commit/fcec85a8094299113a204253d6e5e0ce856949e1))
* **gui:** add shortcuts, status tips and Title Casing to Help menu items ([949ddf1](https://gitlab.com/biomedit/sett/commit/949ddf13d026afcadf4aaa1454269e409610a2cd))
* **versioncheck:** Prevent unnecessary redirect ([b77aa3f](https://gitlab.com/biomedit/sett/commit/b77aa3f3db360658705166e68540a516e3c814cf))

## [1.2.0](https://gitlab.com/biomedit/sett/compare/1.1.2...1.2.0) (2020-07-28)


### Features

* add ascii armored signatures support via update of dependency requirements ([55e8fa6](https://gitlab.com/biomedit/sett/commit/55e8fa650cfddbe20c51d958a69e4d980ba18a19)), closes [#148](https://gitlab.com/biomedit/sett/issues/148)
* add links to documentation and gitlab issues in GUI help menu and CLI ([f737bf4](https://gitlab.com/biomedit/sett/commit/f737bf4c19a0aab99e067913785e9c39c8797b52)), closes [#134](https://gitlab.com/biomedit/sett/issues/134) [#151](https://gitlab.com/biomedit/sett/issues/151)


### Bug Fixes

* **core.crypt:** Provide a version of to_user for generators (e.g. validated_keys_by_ids) ([cc077c0](https://gitlab.com/biomedit/sett/commit/cc077c016b489ffb9d811b8cf38ab3eb248454ec))
* **core.crypt.retrieve_keys:** Remove unnecessary conversion to list ([ea17129](https://gitlab.com/biomedit/sett/commit/ea1712948e59dbbbafcbaf4b1288ef1a8749136b))
* **core.crypt.verify_metadata_signature:** Propagate url_opener ([c306af2](https://gitlab.com/biomedit/sett/commit/c306af269823efed5252d4ac7e61c233ede3a656))
* **GUI:** show PGP keys details by default when doing key management operations ([3a3fd8d](https://gitlab.com/biomedit/sett/commit/3a3fd8d27da2a044f02c1a967f2d2359df7f7198)), closes [#133](https://gitlab.com/biomedit/sett/issues/133)
* **workflows.encrypt:** Add check to ensure that either a sender is specified or a default sender can be determined ([a153b1f](https://gitlab.com/biomedit/sett/commit/a153b1f28f74ece2c745cfc1f3c95bc9c33c3aa3))

### [1.1.2](https://gitlab.com/biomedit/sett/compare/1.1.1...1.1.2) (2020-07-13)


### Bug Fixes

* **core.crypt:** Propagate url_opener parameter to libbiomedit functions ([6dff118](https://gitlab.com/biomedit/sett/commit/6dff118a4f4a5358919a25b59364a8e3a49a31f4))
* **gui.app:** remove explicit keys=() from gpg.GPGStore.search_*_keys calls ([04e8abe](https://gitlab.com/biomedit/sett/commit/04e8abea66b161166befd00815acae7db5dd3d0b))
* **workflows.decrypt:** replace verify_metadata_signature by the corresponding version of libbiomedit ([7c23ac2](https://gitlab.com/biomedit/sett/commit/7c23ac2a644b0122d88bfd67495860312e8d6f11)), closes [#137](https://gitlab.com/biomedit/sett/issues/137)

### 1.1.1 (2020-07-06)
