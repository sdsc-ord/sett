# sett-gui smoke tests

This document lists the manual tests that should be run for `sett-gui` before
and after a new release of the tool is made.

Ideally, the above test should be run on all of the following operating
systems:

* Linux
* Windows 10
* MacOS

In addition of the tests listed below, any new feature or bug fix that is
specific to the new release should also be tested.

*Note:* obviously, only dummy data should be used in these tests.

## 1. Encryption test

Encrypt at least 1 file and 1 directory for 2 or more recipients. The tester's
own key should be included in the recipient list so he/she can then test to
decrypt the output.

Testing steps to perform in the **Encrypt** tab:

* **Add files**: add one or more files to encrypt. Include at least one
  file > 100 MB
* **Add directory**: add one or more directories to encrypt.
* **Recipients**: add 2 or more recipients (including your own key to be able
  to use the package in the decryption process) with the **+** button.
* Enter a **DTR ID** to which you will test transfer the encrypted data
  (requires Internet access and a *valid* **DTR ID**).
  Uncheck **Verify DTR** (in the **Settings** tab) if you do NOT want the
  **DTR ID** to be verified.
* **Output suffix**: set to `test` or anything you like.
* **Output location**: change output file location to a custom location.
* Click on **Test** to test run.
* Click on **Package & Encrypt** to run the full encryption workflow. If you
  specified a **DTR ID**, the name of the generated file should look like
  `<project-code>_20210920T181602_<suffix>.zip`.

## 2. Decryption test

Testing steps to perform in the **Decrypt** tab:

* **Add files:** select the file you encrypted at step 1.
* Check (or uncheck) the **Decompress** checkbox.
* **Output location:** set the decrypted file location to a custom location.
* Click on **Test** to test run.
* Click on **Decrypt selected files**, to run the full decryption workflow
* Verify that the data has been properly decrypted and unpacked to the
  specified location

## 3. File transfer test

Testing steps to perform in the **Transfer** tab:

* **Add files**, to select the file you encrypted at step 1.
* Fill the SFTP **Protocol** connection details for either:
  * Your local BioMedIT node:
    * **User name**: your user name.
    * **Host URL**: node landing zone URL.
    * **Destination directory**: `upload` (*leomed* and *sensa*) or
      `uploads` (*sciCORE*).
  * A local test SFTP server (for setup, see instructions at the end of this
    document):
    * **User name**: `sftp`.
    * **Host URL**: `localhost` or `localhost:2222` if port 22 is mapped to
      port 2222 (e.g. when using podman).
    * **Destination directory**: `upload`.
* Click **Test** to test run.
* Click **Transfer selected files** to transfer the test file.
* Verify that the file was correctly transferred by either:
  * Connecting to your BioMedIT project's project space. E.g. via the guacamole
    remote desktop instance of your project
  * If you have used a local test SFTP server, login to check that the files
    have been properly transferred with: `sftp sftp@localhost` or
    `sftp -P 2222 sftp@localhost`.

*Note:* if transferring to an actual BioMedIT landing zone, make sure to that
you are connected with your VPN so that your IP address is whitelisted with
your local node's firewall.

## 4. Key management test

### 4.1 Generate a new PGP key pair and upload it to the public keyserver

* **Generate a new key pair**. Make sure to use an email address you have
  access to.
* Verify the key is now listed under **Private keys** and **Public keys**.
* **Upload the new key to the keyserver**, after having your key selected in
  the Public keys tab.
  * This uploads the key to the public keyserver at
    [https://keys.openpgp.org](https://keys.openpgp.org).
  * You will receive a verification email from `keyserver@keys.openpgp.org`
    (may take a few minutes).
* Click on the link in the email you received. This will "verify" your PGP key
  and make it findable by its email public.
* To **verify that key has been uploaded** and is now verified, click on the
  **Download keys** symbol and search for the email address associated with
  your key. You should get a confirmation message by sett telling you that the
  key was successfully downloaded.

### 4.2 Delete your published PGP key

* Visit [https://keys.openpgp.org/manage](https://keys.openpgp.org/manage).
* Enter the email address associated to your PGP key.
* You will receive an email with a link to administrate your key (may take a
  few minutes). Click on the link and remove your user ID from your key on the
  keyserver.
* Please note that your actual key will remain on the keyserver and cannot be
  removed. It will however only be findable via its fingerprint.

### 4.3 Delete your local PGP key pair

Your key pair can be **deleted locally** with the following `gpg` commands:

* Identify the key you would like to delete and copy its fingerprint.

```sh
gpg --list-keys
```

* Delete the private key, using the copied fingerprint.

```sh
gpg --delete-secret-keys 66CBA5F45A6260FE068510046899592EF6DDDD31
```

* Delete the public key, using the same fingerprint.

```sh
gpg --delete-keys 66CBA5F45A6260FE068510046899592EF6DDDD31
```

After having deleted the key with the commands above, try pressing the
**Refresh key** in the `sett-gui` interface. The deleted key should disappear
from both the Private keys and Public keys lists.

### 4.4 Download a public key from keyserver

Testing steps to perform in the **Keys** tab:

* **Download keys**, and search for an email address of a registered key (e.g.
  your own). This should download the key to your local machine.
* Verify that the downloaded key is now listed under **Public keys**.
* **Update keys**, after having selected a public key. This will not change
  the key, but you should see a message saying the update was successful. If
  you selected a key which was not stored on the keyserver, you'll see an error
  message like
  `Key [90EBAFDD926AED5FD51107B1A0FE8069F052541A] was not found on the keyserver [https://keys.openpgp.org].`
* **Delete keys**, after having selected a public key.
* Verify that the key has now been removed from the **Public keys** list.

## Deploy a local SFTP test server for file transfers

If you do not have access to a BioMedIT node, or do not wish to transfer files
out of your local machine, you can test transfer files using a SFTP test server
docker container.

Full instructions on how to deploy the container can be found
[here](https://git.dcc.sib.swiss/biwg/container/sftp_test_server).

### Command reminder to build containers from scratch

The example below uses `podman-compose`, but `docker-compose` can be used just
as well.

```sh
# Clone repo:
git clone git@git.dcc.sib.swiss:biwg/container/sftp_test_server.git
cd sftp_test_server

# Start SFTP server and test access:
podman-compose up -d
sftp -P 2222 -i ./sftp_server/sftp_ssh_testkey sftp@localhost

# Stop and delete container when you are done:
podman-compose down
```

### Command reminder when pulling from container registry

```sh
# Login to container registry (only needed if container has to be pulled):
docker login registry.dcc.sib.swiss

#  Pull and run container, copy its private SSH key and test access:
docker run -d -p 22:22 --name sftp_test registry.dcc.sib.swiss/biwg/container/sftp_test_server:latest
docker cp sftp_test:/home/sftp/.ssh/sftp_ssh_testkey .
sftp -i ./sftp_ssh_testkey sftp@localhost

# Stop and delete container:
docker stop sftp_test
docker rm sftp_test
```
