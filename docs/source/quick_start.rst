sett quick start guide
======================

.. _PORTAL_URL: https://portal.dcc.sib.swiss

Initial setup
-------------

#. Install *sett* following the instructions given in
   :ref:`Installing sett on your local computer<installing-sett-page>`.
#. Run ``sett-gui``.
#. If you do not already possess a private/public PGP key pair, go to the
   **Keys** tab and create one following the instructions given in the
   :ref:`Generate a new public/private PGP key pair<section-generate-key>`
   section. You should see your new key listed in the **Private keys** and
   **Public keys** fields of the **Keys** tab.

   .. admonition:: BioMedIT

      Your PGP key must be registered
      with the `BioMedIT portal <PORTAL_URL_>`_ before it can be used to
      encrypt, decrypt, or sign data packages.  Please see the
      :ref:`Register your public PGP key in the BioMedIT portal<section-register-key>`
      section for details.

#. If not already done, download the public PGP key of the recipient(s) to whom
   you intend to send data (or from whom you will receive data). In *sett-gui*,
   go to the **Keys** tab and click on the **Download keys** icon, then search
   for your contact's key by either entering the email or fingerprint
   associated with your contact's PGP key.
#. Just after downloading you contact's PGP key, verify if to make sure that
   it is genuine. This can be done by either:

   * If you are a BioMedIT user: in *sett-gui*, select your contact's key in
     the **Public keys** list (under the **Keys** tab) and check that that its
     status as approved by looking for the green text: "This key is approved".
   * Alternatively, contact the key owner and verify the key **fingerprint**
     with them.


Getting a recipient's PGP key
-----------------------------

In order to encrypt data for a given recipient, you must first obtain a copy of
their public PGP key. Public PGP keys are non-sensitive and can be freely
exchanged.

#. Go to the **Keys** tab of the *sett* interface and click on the
   **Download key** icon.
#. A pop-up will appear, were you can search your recipient's key by email or
   key fingerprint.

.. admonition:: BioMedIT

    **Please make sure that your recipient's key is approved**.
    A green text "This key is approved" should be displayed when
    the key is selected in the **Keys tab** of *sett-gui*. Only approved keys
    can be used within BioMedIT.


*sett-gui* quick start
----------------------

Encrypting data
^^^^^^^^^^^^^^^

#. Go to the **Encrypt** tab of the *sett* interface.
#. Add one or more files and directories to encrypt by clicking the
   **Add files** or **Add directories** icons.
#. Select your own PGP key in the **Sender** field. This is the key that will
   be used to sign the encrypted data.
#. Add one or more **Recipients** by selecting them in the drop-down menu and
   clicking on **+**. These are the keys that will be used to encrypt the data,
   i.e. only these recipients will be able to decrypt the data.

   .. admonition:: BioMedIT

       The selected **Recipients** must be approved
       **Data Managers** of the project for which data is being encrypted.

#. **DTR ID:** specifying a valid **Data Transfer Request ID** is mandatory
   when a data package is transferred into the BioMedIT network. For other
   destinations, the **DTR ID** field can be left empty (or set to any
   arbitrary value), and the **Verify DTR** checkbox must be disabled
   (in the **Settings** tab).

   .. admonition:: BioMedIT

       The **Verify DTR** checkbox should always be enabled, since a valid
       **DTR ID** is required by the BioMedIT network.

#. **Purpose:** purpose of the data transfer, please select either
   ``PRODUCTION`` or ``TEST``, or leave it empty.
   
   .. admonition:: BioMedIT

      This filed is mandatory.
    
#. **Suffix** and **Location:** optionally, a custom suffix can be specified
   and will be appended to the file name. The location where the output file
   should be saved can also be specified.
#. **Compression level** slider: select the amount of compression to apply to
   the input data when packaging it. Compression values range between 0 (no
   compression) and 9 (highest compression). Higher compression level result
   in smaller encrypted output files but require more compute time.
   If compression is not required, e.g. because the input data is already in
   a compressed form, then the compression level should be set to 0 in order
   to speed-up the packaging task.
#. Click **Package & Encrypt** to run the encryption workflow on your data.


Transferring data
^^^^^^^^^^^^^^^^^

#. Go to the **Transfer** tab of the *sett* interface.
#. Select one or more files to transfer using the **Add files** button.
#. Select the transfer **Protocol** to be used (**sftp**, **s3** or
   **liquid files**).
#. Enter the required transfer **Parameters** information depending on the
   selected protocol.

   .. admonition:: BioMedIT

      For transfers into the BioMedIT network the transfer protocol and
      associated parameters are provided by your local BioMedIT node.

#. Click **Transfer selected files** to start transferring your files.


Decrypting data
^^^^^^^^^^^^^^^

#. Go to the **Decrypt** tab of the *sett* interface.
#. Select one or more files to decrypt using the **Add files** button.
#. Specify your desired **Output location**.
#. Click on **Decrypt selected files**.


sett command line quick start
-----------------------------

The main commands to encrypt, transfer and decrypt data with *sett* CLI are
given here. Note that key management is not implemented in the CLI. Please use
*sett-gui* or the ``gpg`` command.

.. code-block:: shell

    # Data encryption:
    sett encrypt --sender <sender key fingerprint or email> --recipient <recipient key fingerprint or email> --dtr-id <data transfer ID> --purpose <purpose> --output <output file/directory name> <files or directories to encrypt>

    # Data transfer.
    # to SFTP server:
    sett transfer --protocol=sftp --protocol-args='{"host": "HOST","username":"USERNAME", "destination_dir":"DIR", "pkey":"PRIVATE_RSA_SSH_KEY"}' <files to transfer>
    # to liquid-files server:
    sett transfer --protocol=liquid_files --protocol-args='{"host": "HOST","subject": "SUBJECT", "message": "MESSAGE","api_key":"APIKEY","chunk_size": 100}' <files to transfer>

    # Data decryption:
    sett decrypt --output_dir=<output directory> <files to decrypt>
