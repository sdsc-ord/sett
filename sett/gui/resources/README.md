# Qt Resource System

The Qt Resource System is a mechanism for using binary files in a Qt application.

Adding new resources:

1. Copy files into a subdirectory under the `resources` directory.
2. Add information about added resources by creating (or modifying) a `.rc` file.
3. Generate Python code containing the added resources, e.g. `pyside6-rcc icons.rc -o rc_icons.py`
4. In order to support both PySide2 and PySide6, modify the import statements
   in the generated Python file (import from `sett.gui.pyside`).

For more information see <https://doc.qt.io/qtforpython/tutorials/basictutorial/qrcfiles.html>.
