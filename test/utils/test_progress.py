import io
import unittest
from typing import Any, List, Tuple, Union, cast
from unittest import mock

from sett.utils import progress


class MockProgress(progress.ProgressInterface):
    """Basic implementation of a ProgressInterface."""

    def __init__(self) -> None:
        self.completed_fraction: float = 0

    def update(self, completed_fraction: float) -> None:
        self.completed_fraction = completed_fraction

    def get_completed_fraction(self) -> float:
        return self.completed_fraction


class TestOpener(unittest.TestCase):
    def test_opener(self) -> None:
        mock_f_obj = mock.MagicMock()
        mock_f_obj.__enter__ = mock.Mock(return_value=42)
        mock_open = mock.Mock(return_value=mock_f_obj)
        with mock.patch("builtins.open", mock_open):
            lazy_f_obj = progress.Opener("/tmp/test.txt", "mode")
            self.assertEqual(lazy_f_obj.name, "/tmp/test.txt")
            self.assertEqual(lazy_f_obj.mode, "mode")
            mock_open.assert_not_called()
            with lazy_f_obj as f:
                self.assertEqual(f, 42)
        mock_open.assert_called_once()
        mock_f_obj.__enter__.assert_called_once()
        mock_f_obj.__exit__.assert_called_once()


class TestProgress(unittest.TestCase):
    def test_scale_progress(self) -> None:
        # Verify that when passing None to the function, None is returned.
        self.assertEqual(progress.scale_progress(None, 0.0, 1), None)

        # Verify the rescaling of a progress bar works as expected.
        p = MockProgress()
        with mock.patch.object(p, "update") as update:
            subprogress = progress.scale_progress(p, -1.0, 1.0)
            assert isinstance(subprogress, progress.ProgressInterface)
            subprogress.update(0.0)
            subprogress.update(0.5)
            subprogress.update(1.0)
            update.assert_has_calls([mock.call(-1.0), mock.call(0.0), mock.call(1.0)])

    @staticmethod
    def test_progress_iter() -> None:
        p = MockProgress()
        n = 3
        with mock.patch.object(p, "update") as update:
            p_iter = progress.progress_iter(range(n), p)
            for _ in p_iter:
                pass
            update.assert_has_calls([mock.call(i / n) for i in range(n + 1)])

    def test_progress_iter_with_sub_progress(self) -> None:
        p = MockProgress()
        n = 3
        rng = (range(n),) * n

        with mock.patch.object(p, "update") as update:
            for sub_rng, subprogress in progress.progress_iter_with_sub_progress(
                rng, p
            ):
                for _ in progress.progress_iter(sub_rng, subprogress):
                    pass

            for call, pos in zip(
                update.call_args_list,
                ((i + j / n) / n for i in range(n) for j in range(n + 1)),
            ):
                (arg,), _ = call
                self.assertAlmostEqual(arg, pos)

    def test_subprogress(self) -> None:
        # Create a simple implementation of a progress interface.
        class SimpleProgressBar(progress.ProgressInterface):
            def __init__(self) -> None:
                self._completed_fraction = 0.0
                self._label = ""

            def update(self, completed_fraction: float) -> None:
                self._completed_fraction = completed_fraction

            def get_completed_fraction(self) -> float:
                return self._completed_fraction

        # Create a new instance of a simple progress bar.
        p_bar = SimpleProgressBar()
        completion_values = (0, 0.1, 0.5, 0.7, 1)
        subprogress_step_values = (0.5, 0.3, 0.2)
        step_nb = 0

        # Loop through the 2 subprogress steps of the progress bar.
        for increase in subprogress_step_values:
            step_nb += 1
            # Compute start and stop points for the current subprogress bar.
            # We use these values to test that the subprogress bar returns
            # the correct values.
            start = sum(subprogress_step_values[: step_nb - 1])
            stop = start + increase

            # Initialize subprogress bar.
            with progress.subprogress(
                progress=p_bar, step_completion_increase=increase
            ) as subprogress:
                assert isinstance(subprogress, progress.ProgressInterface)
                for x in completion_values:
                    subprogress.update(x)
                    # Verify the completed fraction of the global progress
                    # bar is correct.
                    expected = start + x * (stop - start)
                    self.assertEqual(
                        p_bar.get_completed_fraction(),
                        expected,
                        f"global fraction should be {expected}",
                    )
                    # Verify the completed fraction of the subprogress bar
                    # is correct. The completed fraction
                    self.assertAlmostEqual(
                        subprogress.get_completed_fraction(),
                        x,
                        msg=f"subprogress fraction should be {x}",
                    )

        # Test the function when progress is None.
        p_none = None
        with progress.subprogress(progress=p_none, step_completion_increase=1) as _:
            pass

    def test_with_progress(self) -> None:
        p = MockProgress()
        with mock.patch.object(p, "update") as update:
            streams: List[Tuple[Union[bytes, str], type]] = [
                (".", io.StringIO),
                (b".", io.BytesIO),
            ]
            for content, IoType in streams:
                with self.subTest(type=IoType):
                    f_progress = progress.with_progress(IoType(content * 10), p)
                    self.assertEqual(f_progress.read(3), content * 3)
                    update.assert_called_once_with(3 / 10)
                    update.reset_mock()
                    self.assertEqual(f_progress.read(), content * 7)
                    update.assert_called_once_with(1.0)
                    update.reset_mock()

            p_wp = progress.with_progress(cast(io.BytesIO, io.StringIO("1\n2\n3\n")), p)
            self.assertEqual(p_wp.readlines(), ["1\n", "2\n", "3\n"])
            update.assert_called_once_with(1.0)
            update.reset_mock()

            lines = cast(io.BytesIO, io.StringIO("1\n2\n3\n"))
            for i, line in enumerate(progress.with_progress(lines, p), 1):
                self.assertEqual(line, str(i) + "\n")
                update.assert_called_once_with(i / 3)
                update.reset_mock()

    def assertListAlmostEqual(
        self, list1: Union[List[Any], Tuple[Any, ...]], list2: List[Any], tol: int
    ) -> None:
        self.assertEqual(len(list1), len(list2))
        for a, b in zip(list1, list2):
            if isinstance(a, (list, tuple)):
                self.assertListAlmostEqual(a, b, tol)
            else:
                self.assertAlmostEqual(a, b, tol)

    def test_weights_to_ranges(self) -> None:
        for weights, expected in (
            ([2, 1, 1], [(0.0, 0.5), (0.5, 0.75), (0.75, 1.0)]),
            ([1], [(0.0, 1.0)]),
            ([0], [(0.0, 1.0)]),
            ([0, 0], [(0.0, 0.5), (0.5, 1.0)]),
        ):
            ranges = list(progress.weights_to_ranges(weights))
            self.assertEqual(len(ranges), len(weights))
            self.assertListEqual(ranges, expected)
        for weights, expected in (
            ([20, 0], [(0.0, 0.99), (0.99, 1.0)]),
            ([0, 20], [(0.0, 0.05), (0.05, 1.0)]),
        ):
            ranges = list(progress.weights_to_ranges(weights))
            self.assertEqual(len(ranges), len(weights))
            self.assertListAlmostEqual(ranges, expected, 1)
