from unittest import mock
import pytest

from sett.core import crypt
from sett.core.error import UserError
from sett.core.secret import Secret


def test_enforce_password() -> None:
    s = Secret("topsecret")
    assert crypt.enforce_passphrase(s) == s
    assert isinstance(crypt.enforce_passphrase(s), Secret)
    with pytest.raises(ValueError):
        crypt.enforce_passphrase(None)


@mock.patch("sett.core.crypt.check_password")
def test_check_password_matches_any_key_good(mock_check_password: mock.Mock) -> None:
    key1 = mock.Mock()
    key1.fingerprint = "AAAA"
    keys = [key1, mock.Mock()]
    p = Secret("test")
    store = mock.Mock()
    crypt.check_password_matches_any_key(p, keys, store)
    mock_check_password.assert_called_once_with(
        key_fingerprint=key1.fingerprint, password=p, gpg_store=store
    )


@mock.patch("sett.core.crypt.check_password")
def test_check_password_matches_any_key_wrong_password(
    mock_check_password: mock.Mock,
) -> None:
    mock_check_password.side_effect = UserError
    with pytest.raises(UserError) as excinfo:
        crypt.check_password_matches_any_key(
            Secret(""), [mock.Mock(), mock.Mock()], mock.Mock()
        )
    assert format(excinfo.value) == (
        "The provided password does not match any of the GPG keys with "
        "which the data was encrypted"
    )
