import itertools
import unittest
from pathlib import Path
from unittest.mock import patch, call, Mock

from sett.core import error
from sett.core.error import UserError
from sett.core.filesystem import (
    delete_file_on_error,
    get_compression_stats,
    search_files_recursively,
    delete_files,
    DeleteDirOnError,
    to_human_readable_size,
    unique_filename,
)


class TestFilesystem(unittest.TestCase):
    def setUp(self) -> None:
        self.file_name = "test_filename"
        self.file_extension = ".txt"
        self.default_separator = "_"
        self.separator = ","

    def test_search_files_recursively_with_non_existing_path(self) -> None:
        with self.assertRaises(error.UserError):
            list(search_files_recursively(list("does_not_exist")))

    def test_search_files_recursively(self) -> None:
        docs = Path(__file__).parent.parent.parent / "docs"
        f = Path(__file__)
        self.assertTrue(docs.exists())
        found = list(search_files_recursively(list([str(docs), str(f)])))
        reference = [str(p.absolute()) for p in docs.rglob("*") if p.is_file()]
        reference.append(str(f.absolute()))
        self.assertSetEqual(set(found), set(reference))
        self.assertEqual(len(found), len(reference))

    @staticmethod
    def test_delete_files_with_non_existing_paths() -> None:
        delete_files("does_not_exist_1", "does_not_exist_2")

    @patch("sett.core.filesystem.os")
    @patch("sett.core.filesystem.os.path")
    def test_delete_files(self, mock_path: Mock, mock_os: Mock) -> None:
        f = ["1", "2"]
        mock_path.exists.return_value = True
        delete_files(*f)
        self.assertEqual(mock_os.unlink.call_count, 2)

    @patch("sett.core.filesystem.os.path")
    def test_delete_files_failed(self, mock_path: Mock) -> None:
        mock_path.exists.return_value = True
        with self.assertWarns(UserWarning):
            # As '1' does NOT exist, 'unlink' will fail
            delete_files("1")

    @patch("os.path.exists", return_value=False)
    def test_unique_filename_not_existing(self, _: Mock) -> None:
        # Default arguments, filename remains unchanged
        self.assertEqual(self.file_name, unique_filename(self.file_name))
        # Different extension
        self.assertEqual(
            f"{self.file_name}{self.file_extension}",
            unique_filename(self.file_name, self.file_extension),
        )
        # Different separator
        self.assertEqual(
            f"{self.file_name}{self.file_extension}",
            unique_filename(self.file_name, self.file_extension, self.separator),
        )

    @patch("os.path.exists", side_effect=itertools.cycle([True, False]))
    def test_unique_filename_existing_1(self, _: Mock) -> None:
        # Default arguments
        self.assertEqual(
            f"{self.file_name}{self.default_separator}1",
            unique_filename(self.file_name),
        )
        # Different extension
        self.assertEqual(
            f"{self.file_name}{self.default_separator}1{self.file_extension}",
            unique_filename(self.file_name, self.file_extension),
        )
        # Different extension and separator
        self.assertEqual(
            f"{self.file_name}{self.separator}1{self.file_extension}",
            unique_filename(self.file_name, self.file_extension, self.separator),
        )
        # Start from 0
        self.assertEqual(
            f"{self.file_name}{self.default_separator}0{self.file_extension}",
            unique_filename(self.file_name, self.file_extension, first_number=0),
        )

    @patch("os.path.exists", side_effect=[True, True, False])
    def test_unique_filename_existing_2(self, _: Mock) -> None:
        self.assertEqual(
            f"{self.file_name}{self.default_separator}2",
            unique_filename(self.file_name),
        )


class TestDeleteDirOnError(unittest.TestCase):
    def setUp(self) -> None:
        self.test_dir = "/test/directory"
        self.logger_name = "sett.core.filesystem"
        self.log_level = "DEBUG"

    def trigger_exception(self) -> None:
        try:
            with DeleteDirOnError(self.test_dir) as output:
                raise UserError
        except UserError:
            pass  # context manager was triggered
        self.assertEqual(self.test_dir, output)

    @patch("os.path.isdir", return_value=False)
    @patch("shutil.rmtree")
    def test_directory_not_existing(self, mock_rmtree: Mock, mock_isdir: Mock) -> None:
        with self.assertLogs(self.logger_name, level=self.log_level) as cm:
            self.trigger_exception()

        assert cm is not None
        self.assertIn(f"Directory doesn't exist: {self.test_dir}", cm.output[0])
        mock_isdir.assert_called_with(self.test_dir)
        mock_rmtree.assert_not_called()

    @patch("os.path.isdir", return_value=True)
    @patch("shutil.rmtree")
    def test_directory_existing(self, mock_rmtree: Mock, mock_isdir: Mock) -> None:
        with self.assertLogs(self.logger_name, level=self.log_level) as cm:
            self.trigger_exception()

        assert cm is not None
        self.assertIn(f"Deleting directory: {self.test_dir}", cm.output[0])
        mock_isdir.assert_called_with(self.test_dir)
        mock_rmtree.assert_called_with(self.test_dir)

    def test_no_exception_no_action(self) -> None:
        try:
            with self.assertLogs(self.logger_name, level=self.log_level) as cm:
                with DeleteDirOnError(self.test_dir) as output:
                    pass
        except AssertionError:
            # expected exception: AssertionError: no logs of level DEBUG or higher triggered
            pass

        assert cm is not None
        self.assertEqual(0, len(cm.output))
        self.assertEqual(self.test_dir, output)


class TestDeleteFileOnError(unittest.TestCase):
    def setUp(self) -> None:
        self.path = "test/path"

    @patch("os.remove")
    def test_normal_operation(self, mock_remove: Mock) -> None:
        with delete_file_on_error(self.path):
            pass
        mock_remove.assert_not_called()

    @patch("os.path.exists", return_value=True)
    @patch("os.remove")
    def test_file_deletion(self, mock_remove: Mock, mock_exists: Mock) -> None:
        with self.assertRaises(UserError), self.assertLogs(
            f"Deleting file: {self.path}", level="DEBUG"
        ), delete_file_on_error(self.path):
            raise UserError
        mock_exists.assert_has_calls([call(self.path)])
        mock_remove.assert_called_once_with(self.path)

    @patch("os.path.exists", return_value=False)
    @patch("os.remove")
    def test_file_does_not_exist(self, mock_remove: Mock, mock_exists: Mock) -> None:
        with self.assertRaises(UserError), self.assertLogs(
            f"File doesn't exist: {self.path}", level="DEBUG"
        ), delete_file_on_error(self.path):
            raise UserError
        mock_exists.assert_has_calls([call(self.path)])
        mock_remove.assert_not_called()


class TestFileSize(unittest.TestCase):
    def test_to_human_readable_size(self) -> None:
        self.assertEqual(to_human_readable_size(42, sep="_"), "42_B")
        prefix = ("", "k", "M", "G", "T", "P", "E", "Z", "Y")
        sizes = (
            9_253,
            3_771_286,
            8_363_220_129,
            7_856_731_783_569,
            4_799_178_968_842_384,
            3_424_799_178_968_842_384,
            4_799_178_324_968_491_842_384,
            74_808_380_225_000_602_624_000_000,
        )
        for i, size in enumerate(sizes, 1):
            self.assertEqual(
                to_human_readable_size(size), f"{round(size / 1024**i, 2)} {prefix[i]}B"
            )

    def test_get_compression_stats(self) -> None:
        for source, output, compression, ratio in (
            (100, 42, True, "2.38"),
            (42, 100, True, "0.42"),
            (123456, 12345, True, "10.0"),
            (100, 42, False, "0.42"),
            (42, 100, False, "2.38"),
            (123456, 12345, False, "0.1"),
        ):
            self.assertEqual(
                get_compression_stats(source, output, compression),
                f"source: {to_human_readable_size(source)}, "
                f"output: {to_human_readable_size(output)}, "
                f"compression ratio: {ratio}",
            )
