import hashlib
import io
import unittest
from typing import Any, Tuple, Optional
from unittest import mock

from sett.core import checksum, error


def mock_as_posix(file_path: str) -> str:
    return str(file_path).replace("\\", "/")


class BytesIOWithName(io.BytesIO):
    def __init__(self, binary_content: bytes, file_name: str, *args: Any) -> None:
        self.name = file_name
        io.BytesIO.__init__(self, binary_content, *args)


class TestChecksum(unittest.TestCase):
    files = ["/test/file1.txt", "/test/file2.txt"]
    pkg_files = ("content/file1.txt", "content/file2.txt")
    content = [
        b"Mock an item where it is used, not where it came from.\n"
        b"Mock an item where it is used, not where it came from.",
        b"This is the content of the second file.",
    ]
    content_checksum = (
        "7ce0ad43befbf2b2e43fb108184ca6a4e3bd8ab8e05296d7890c523ad76d2bc7",
        "860282aecc2a0b9efc6356aef13b56d46f26fad8824a3950dfea138ccf0e66c6",
    )

    def test_read_checksum_file(self) -> None:
        ref_checks = [
            ("a7186ae7ff993b379qcf3567775cfc71a212rf217e4dd", "testDir/file1.fastq"),
            (
                "a7186ae7ff993b379qcf3567775cfc71a212rf217e4dd",
                "testDir/file with spaces.fastq",
            ),
            ("f8d2d394264823e711fgc34e4ac83f8cbc253c6we034f", "testDir/file2.fastq"),
            (
                "78f3b23fe49cf5f7f245ddf43v9788d9e62c0971fe5fb",
                "testDir/subdir2/file4.fastq",
            ),
        ]
        checksum_file_object = io.BytesIO(
            b"\n".join(sha.encode() + b" " + name.encode() for sha, name in ref_checks)
        )
        checks = list(checksum.read_checksum_file(checksum_file_object))
        self.assertEqual(checks, ref_checks)

        checksum_file_object = io.BytesIO(b".........")
        with self.assertRaises(error.UserError):
            list(checksum.read_checksum_file(checksum_file_object))

    def test_compute_checksum_sha256(self) -> None:
        text = self.content[0]
        expected_hash = self.content_checksum[0]
        hash1 = checksum.compute_checksum_sha256(file_object=io.BytesIO(text))
        hash2 = checksum.compute_checksum_sha256(
            file_object=io.BytesIO(text), block_size=5
        )
        self.assertEqual(
            hash1, expected_hash, f"global fraction should be {expected_hash}"
        )
        self.assertEqual(
            hash2, expected_hash, f"global fraction should be {expected_hash}"
        )

    def test_verify_checksum(self) -> None:
        with self.subTest("Checksum matches"), mock.patch(
            "sett.core.checksum.open", mock.mock_open(read_data=self.content[0])
        ) as m:
            self.assertIsNone(
                checksum.verify_checksum((self.content_checksum[0], self.files[0]))
            )
            m.assert_called_once_with(self.files[0], "rb")

        wrong_content = b"garbage content"
        with self.subTest("Checksum does not match"), mock.patch(
            "sett.core.checksum.open", mock.mock_open(read_data=wrong_content)
        ) as m:
            self.assertEqual(
                checksum.verify_checksum((self.content_checksum[1], self.files[1])),
                hashlib.sha256(wrong_content).hexdigest(),
            )
            m.assert_called_once_with(self.files[1], "rb")

    # This method is necessary since Mock can't be pickled for multiprocessing
    @staticmethod
    def verify_checksum_ok_mock(_: Tuple[str, str]) -> Optional[str]:
        return None

    # This method is necessary since Mock can't be pickled for multiprocessing
    @staticmethod
    def verify_checksum_mismatch_mock(_: Tuple[str, str]) -> Optional[str]:
        return "hash_mismatch"

    def test_verify_checksums(self) -> None:
        with self.subTest("Checksums match"), mock.patch(
            "sett.core.checksum.verify_checksum", self.verify_checksum_ok_mock
        ):
            checksum.verify_checksums(entries=(("hash", "path"),))

        with self.subTest("Checksums match"), mock.patch(
            "sett.core.checksum.verify_checksum", self.verify_checksum_mismatch_mock
        ), self.assertRaises(error.UserError) as error_cm:
            checksum.verify_checksums(entries=(("hash", "path/to/file.txt"),))
        self.assertEqual(
            "Checksum mismatch for: path/to/file.txt (expected: hash, got: hash_mismatch)",
            format(error_cm.exception),
        )

    def test_compute_file_checksum_sha256(self) -> None:
        with mock.patch(
            "sett.core.checksum.open", mock.mock_open(read_data=self.content[0])
        ) as m:
            self.assertEqual(
                checksum.compute_file_checksum_sha256(self.files[0]),
                self.content_checksum[0],
            )
            m.assert_called_once_with(self.files[0], "rb")

    # This method is necessary since Mock can't be pickled for multiprocessing
    @classmethod
    def compute_file_checksum_sha256_mock(cls, path: str) -> str:
        return cls.content_checksum[cls.files.index(path)]

    def test_generate_checksums_file_content(self) -> None:
        with mock.patch(
            "sett.core.checksum.compute_file_checksum_sha256",
            self.compute_file_checksum_sha256_mock,
        ):
            self.assertEqual(
                checksum.generate_checksums_file_content(
                    zip(self.pkg_files, self.files)
                ),
                (
                    "".join(
                        f"{cs} {name}\n"
                        for cs, name in zip(self.content_checksum, self.pkg_files)
                    )
                ).encode(),
            )

    def test_generate_checksums_file_content_on_win(self) -> None:
        # On others, '\\' should be converted into '/'
        with mock.patch("sett.core.checksum.os.path.sep", "\\"), mock.patch(
            "sett.core.checksum.Path.as_posix", mock_as_posix
        ), mock.patch(
            "sett.core.checksum.compute_file_checksum_sha256",
            self.compute_file_checksum_sha256_mock,
        ):
            checksums = checksum.generate_checksums_file_content(
                zip((name.replace("/", "\\") for name in self.pkg_files), self.files)
            )
            self.assertEqual(
                checksums,
                (
                    "".join(
                        f"{cs} {name}\n"
                        for cs, name in zip(self.content_checksum, self.pkg_files)
                    )
                ).encode(),
            )
