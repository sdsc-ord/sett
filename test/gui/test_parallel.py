import unittest
from unittest import mock

from typing import Any, Callable, Optional, Dict


from sett.gui.parallel import run_thread
from sett.gui.pyside import QtCore


class ThreadCallback:
    """For capturing calls outside of threads"""

    def __init__(self, f: Callable[..., Any]):
        self.local_call = mock.Mock(side_effect=f)
        self.thread_callback = mock.create_autospec(f, side_effect=self.local_call)


class MockSignal:
    """QtCore.Signal seem not to work properly within this test environment"""

    def __init__(self) -> None:
        self.callback: Optional[Callable[..., Any]] = None

    def connect(self, callback: Callable[..., Any]) -> None:
        self.callback = callback

    def emit(self, *args: Any, **kwargs: Any) -> None:
        if self.callback is None:
            return
        self.callback(*args, **kwargs)


class MockWorkerSignals:
    """Replaces gui.parallel.WorkerSignals replacing QtCore.Signal with MockSignal"""

    def __init__(self) -> None:
        self.signals: Dict[str, MockSignal] = {}

    def __getattr__(self, attr: str) -> Any:
        return self.signals.setdefault(attr, MockSignal())


@mock.patch("sett.gui.parallel.WorkerSignals", MockWorkerSignals)
class TestRunThread(unittest.TestCase):
    def setUp(self) -> None:
        @ThreadCallback
        def job(x: int, s: str) -> int:
            assert x is not None
            assert s is not None
            return 2

        self.job = job

        @ThreadCallback
        def on_done(result: Any) -> None:
            assert result

        self.on_done = on_done

        @ThreadCallback
        def on_warning(msg: str) -> None:
            assert msg is not None

        self.on_warning = on_warning

        @ThreadCallback
        def on_error(msg: str) -> None:
            assert msg is not None

        self.on_error = on_error

        @ThreadCallback
        def on_finished() -> None:
            pass

        self.on_finished = on_finished

    @mock.patch("sett.utils.error_handling.error_report_on_exception")
    def test_run_thread(self, on_error_report: Any) -> None:
        config = mock.Mock(error_reports=False)
        logger = mock.Mock()

        run_thread(
            self.job.thread_callback,
            f_kwargs={"x": 1, "s": "A"},
            forward_errors=self.on_error.thread_callback,
            signals={
                "warning": self.on_warning.thread_callback,
                "result": self.on_done.thread_callback,
                "finished": self.on_finished.thread_callback,
            },
            capture_loggers=(logger,),
            report_config=config,
        )
        QtCore.QThreadPool.globalInstance().waitForDone()
        self.job.local_call.assert_called_once_with(x=1, s="A")
        self.on_warning.local_call.assert_not_called()
        self.on_error.local_call.assert_not_called()
        self.on_done.local_call.assert_called_once_with(2)
        self.on_finished.local_call.assert_called_once_with()
        on_error_report.assert_not_called()

    @mock.patch("sett.utils.error_handling.error_report_on_exception")
    def test_run_thread_with_exception(self, on_error_report: Any) -> None:
        config = mock.Mock(error_reports=False)
        logger = mock.Mock()
        exception = RuntimeError("💥")

        def job(x: int, s: str) -> None:
            assert x is not None
            assert s is not None
            raise exception

        run_thread(
            job,
            f_kwargs={"x": 1, "s": "A"},
            forward_errors=self.on_error.thread_callback,
            signals={
                "warning": self.on_warning.thread_callback,
                "result": self.on_done.thread_callback,
                "finished": self.on_finished.thread_callback,
            },
            capture_loggers=(logger,),
            report_config=config,
        )
        QtCore.QThreadPool.globalInstance().waitForDone()
        self.on_warning.local_call.assert_not_called()
        self.on_error.local_call.assert_called_once_with(exception)
        self.on_done.local_call.assert_not_called()
        self.on_finished.local_call.assert_called_once_with()
        on_error_report.assert_not_called()
