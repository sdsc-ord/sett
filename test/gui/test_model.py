import unittest
from typing import Any
from unittest import mock

from sett.gui.model import TableModel, KeyValueListModel, ConfigProxy
from sett.gui.pyside import QtCore
from sett.utils.config import Config

DISPLAY_ROLE: Any = QtCore.Qt.ItemDataRole.DisplayRole


class TestGuiModel(unittest.TestCase):
    def setUp(self) -> None:
        self.model = TableModel(data=[[1, 2], [2, 3]], columns=("A", "B"))

    def test_that_row_count_works(self) -> None:
        self.assertEqual(self.model.rowCount(), 2)
        self.assertEqual(TableModel(data=[]).rowCount(), 0)

    def test_that_col_count_works(self) -> None:
        self.assertEqual(TableModel(data=[[1, 2, 3]]).columnCount(), 3)
        self.assertEqual(TableModel(data=[]).columnCount(), 0)

    def test_that_model_header_data_works(self) -> None:
        with self.subTest("horizontal"):
            self.assertEqual(
                self.model.headerData(
                    1, QtCore.Qt.Orientation.Horizontal, DISPLAY_ROLE
                ),
                "B",
            )
        with self.subTest("vertical"):
            self.assertEqual(
                self.model.headerData(1, QtCore.Qt.Orientation.Vertical, DISPLAY_ROLE),
                "1",
            )
        with self.subTest("not Qt.DisplayRole"):
            self.assertEqual(
                self.model.headerData(
                    1, QtCore.Qt.Orientation.Vertical, DISPLAY_ROLE + 1
                ),
                None,
            )

    def test_that_model_data_works(self) -> None:
        index = self.model.createIndex(0, 1)
        with self.subTest("Qt.DisplayRole"):
            self.assertEqual(
                self.model.data(index, role=DISPLAY_ROLE),
                2,
            )
        with self.subTest("not Qt.DisplayRole"):
            self.assertEqual(
                self.model.data(index, role=DISPLAY_ROLE + 1),
                None,
            )

    def test_that_model_remove_rows_works(self) -> None:
        with self.subTest("valid range"):
            model = TableModel(data=[[1, 2], [2, 3]])
            self.assertEqual(
                model.get_data(),
                ((1, 2), (2, 3)),
            )
            self.assertTrue(model.removeRows(0, 1))
            self.assertEqual(model.get_data(), ((2, 3),))
        with self.subTest("invalid range"):
            model = TableModel(data=[[1, 2]])
            self.assertEqual(model.get_data(), ((1, 2),))
            self.assertFalse(model.removeRows(0, 3))
            self.assertFalse(model.removeRows(-1, 1))
            self.assertEqual(model.get_data(), ((1, 2),))


class TestGuiKeyValueListModel(unittest.TestCase):
    def setUp(self) -> None:
        self.model = KeyValueListModel(data=[("a", 1), ("b", 2)])

    def test_that_row_count_works(self) -> None:
        self.assertEqual(self.model.rowCount(), 2)
        self.assertEqual(KeyValueListModel(data=[]).rowCount(), 0)

    def test_that_data_works(self) -> None:
        index = self.model.createIndex(0, 1)
        with self.subTest("Qt.DisplayRole"):
            self.assertEqual(
                self.model.data(index, role=DISPLAY_ROLE),
                "a",
            )
        with self.subTest("not Qt.DisplayRole"):
            self.assertEqual(
                self.model.data(index, role=DISPLAY_ROLE + 1),
                None,
            )

    def test_that_get_value_works(self) -> None:
        with self.subTest("int"):
            self.assertEqual(self.model.get_value(1), 2)
        with self.subTest("QtCore.QModelIndex"):
            self.assertEqual(self.model.get_value(self.model.createIndex(0, 0)), 1)
        with self.subTest("invalid type"):
            with self.assertRaises(TypeError):
                key: Any = ...
                self.model.get_value(key)


class TestConfigProxy(unittest.TestCase):
    def setUp(self) -> None:
        self.config = Config()
        self.proxy = ConfigProxy(self.config)

    def test_that_config_proxy_set_value_works(self) -> None:
        self.proxy.set_value("dcc_portal_url", "xxx")
        self.assertEqual(self.config.dcc_portal_url, "xxx")
        self.assertEqual(self.proxy.dcc_portal_url, "xxx")

    def test_that_config_proxy_refresh_works(self) -> None:
        mock_callback = mock.Mock()
        self.proxy.add_listener("dcc_portal_url", mock_callback)
        self.proxy.refresh()
        mock_callback.assert_called_once()

    def test_that_config_proxy_set_config_works(self) -> None:
        mock_callback = mock.Mock()
        self.proxy.add_listener("dcc_portal_url", mock_callback)
        new_config = Config(dcc_portal_url="yyy")
        self.proxy.set_config(new_config)
        mock_callback.assert_called_once()
        self.assertEqual(self.proxy, new_config)
