import unittest
from unittest import mock

from dataclasses import dataclass

from sett.gui.listener import ClassWithListener, ListenerWrap


class TestClassWithListener(unittest.TestCase):
    @staticmethod
    def test_that_setitem_works() -> None:
        @dataclass
        class X(ClassWithListener):
            a: int
            b: str

            def __post_init__(self) -> None:
                super().__init__()

        x = X(1, "...")
        callback_a = mock.Mock()
        callback_b = mock.Mock()
        x.add_listener("a", callback_a)
        x.add_listener("b", callback_b)
        x.a = 1
        callback_a.assert_called_once()
        callback_b.assert_not_called()


class TestListenerWrap(unittest.TestCase):
    @staticmethod
    def test_that_setitem_works() -> None:
        @dataclass
        class X:
            a: int
            b: str

        x = ListenerWrap(X(1, "..."))
        callback_a = mock.Mock()
        callback_b = mock.Mock()
        x.add_listener("a", callback_a)
        x.add_listener("b", callback_b)
        x.target = X(2, "...")
        callback_a.assert_called_once()
        callback_b.assert_called_once()
