import factory
from gpg_lite import Key, Uid, model
from pytest_factoryboy import register
from sett.core.crypt import KeyAlgorithm


@register
class KeyFactory(factory.Factory):  # type: ignore
    class Meta:
        model = Key

    key_id = ("12345",)
    key_type = (model.KeyType.public,)
    fingerprint = ("XYZ",)
    creation_date = ("",)
    uids = ((Uid(email="", full_name=""),),)
    key_length = 4096
    pub_key_algorithm = KeyAlgorithm.RSA
